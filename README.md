## Environment

web server: apache or nginx (if run over web)\
docker: (if run on docker)\
php: 8.*

Pass params to script over config.php

## Instalation

1. Clone this repository\
   `git clone https://gitlab.com/abratka/databank.git [project folder]`


2. Configure apache or nginx for prtoject\
   root is `./`

## Run with console

1. Go to project folder


2. Just run in terminal:\
   `php console/index.php`


## Set up with docker

1. Go to project folder


2. Run `docker-compose up -d`


3. Project url http://localhost:8080
