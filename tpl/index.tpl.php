<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $config['title']; ?></title>
    <link href="../css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <link href="../css/app.css" rel="stylesheet" crossorigin="anonymous">
</head>
<body>
    <div class="container py-5">
        <div class="display-5 pb-3 text-uppercase">
            <?= $config['title']; ?>
        </div>
        <h5>Prime numbers</h5>
        <hr>
        <p><?= implode(', ', $primes) ?></p>
        <h5>Palindrome numbers</h5>
        <hr>
        <p><?= implode(', ', $palindromes) ?></p>
        <h5>Passwords</h5>
        <hr>
        <div class="row">
            <h6>Password</h6>
            <p><?= $password ?></p>
        </div>
        <div class="row">
            <h6>md5</h6>
            <p><?= md5($password) ?></p>
        </div>
        <div class="row">
            <h6>sha1</h6>
            <p><?= sha1($password) ?></p>
        </div>
    </div>
</body>
</html>
