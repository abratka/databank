<?php

global $config;

require_once './config.php';
require_once './inc/numbers.php';
require_once './inc/template.php';

$numbers = new Numbers($config);

$template = new Template('tpl');

echo $template->render('index.tpl.php', [
    'config' => $config,
    'primes' => $numbers->getPrimes(),
    'palindromes' => $numbers->getPalindromes(),
    'password' => $numbers->getPassword(),
]);