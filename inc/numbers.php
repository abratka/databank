<?php

class Numbers
{
    /**
     * @var array
     */
    private array $config = [];

    /**
     * @var array
     */
    private array $numbers = [];

    /**
     * @var array
     */
    private array $primes = [];

    /**
     * @var array
     */
    private array $palindromes = [];

    /**
     * @var ?string
     */
    private ?string $password = null;

    /**
     * Constructor
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setNumbers();
        $this->setPrimes();
        $this->setPalindromes();
        $this->setPassword();
    }

    /**
     * Set numbers.
     *
     * @return $this
     */
    public function setNumbers(): static
    {
        $this->numbers = range(1, $this->config['limit']);

        return $this;
    }

    /**
     * Get numbers.
     *
     * @return array
     */
    public function getNumbers(): array
    {
        return $this->numbers;
    }

    /**
     * Set primes numbers.
     *
     * @return $this
     */
    public function setPrimes(): static
    {
        foreach ($this->numbers as $num) {
            if ($this->checkPrime($num)) {
                $this->primes[] = $num;
            }
        }

        return $this;
    }

    /**
     * Get primes numbers.
     *
     * @return array
     */
    public function getPrimes(): array
    {
        return $this->primes;
    }

    /**
     * Set palindrome numbers.
     *
     * @return $this
     */
    public function setPalindromes(): static
    {
        foreach ($this->numbers as $num) {
            if ($this->checkPalindrome($num)) {
                $this->palindromes[] = $num;
            }
        }

        return $this;
    }

    /**
     * Get palindrome numbers.
     *
     * @return array
     */
    public function getPalindromes(): array
    {
        return $this->palindromes;
    }

    /**
     * Set password.
     *
     * @return $this
     */
    public function setPassword(): static
    {
        $password = [];

        for ($i = 0; $i < count($this->primes); $i++) {
            $password[] = rand(1, $this->primes[$i]);
        }

        $this->password = substr(implode('', $password), 0, $this->config['length']);

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Check if number is prime number.
     *
     * @param $num
     * @return bool
     */
    private function checkPrime($num): bool
    {
        if ($num == 1) {
            return false;
        }

        for ($i = 2; $i <= $num / 2; $i++) {
            if ($num % $i == 0) {
                return false;
            }
        }

        return true;
    }

    /**]
     * Check if number is palindrome number.
     *
     * @param $num
     * @return bool
     */
    private function checkPalindrome($num): bool
    {
        $reverse = (int) strrev($num);

        if ($reverse === $num) {
            return true;
        }

        return false;
    }
}
