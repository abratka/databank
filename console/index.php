<?php

global $config;

require_once './config.php';
require_once './inc/numbers.php';
require_once './inc/template.php';

$numbers = new Numbers($config);

echo "Prime numbers\n";
echo implode(', ', $numbers->getPrimes());
echo "\n";

echo "Palindrome numbers\n";
echo implode(', ', $numbers->getPalindromes());
echo "\n";

echo "Password\n";
echo $numbers->getPassword();
echo "\n";

echo "md5\n";
echo md5($numbers->getPassword());
echo "\n";

echo "sha1\n";
echo sha1($numbers->getPassword());
echo "\n";